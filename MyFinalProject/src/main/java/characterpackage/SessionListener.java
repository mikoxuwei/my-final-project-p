/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package characterpackage;

import com.mycompany.myfinalproject.User;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author miko
 */
public class SessionListener implements HttpSessionListener {
    
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        List<Character> characters = new ArrayList<>();
        se.getSession().setAttribute("characters", characters);
        List<User> users = new ArrayList<>();
        se.getSession().setAttribute("users", users);
        HttpSession session = se.getSession();
        ServletContext servletContext = session.getServletContext();
        servletContext.log("user logged in at "+session.toString());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        ServletContext servletContext = session.getServletContext();
        servletContext.log("user logged out");
    }
}
