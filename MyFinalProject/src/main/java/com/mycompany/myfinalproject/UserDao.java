/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.myfinalproject;

import characterpackage.AddCharacterServlet;
import characterpackage.Character;
import characterpackage.CharacterListServlet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author miko
 */
public class UserDao {
    static int e;
    // e=0; 沒錯， e=1; 帳號創建成功
    // e=2; 帳號有人使用
    // e=3; 查無帳號  e=4; 密碼錯誤。
    
    public int gete(int ne){
        e = ne;
        return e;
    }
    public int takee(){
        return e;
    }
    
    public void addUser(User user) {
        try ( Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "");) {
            String id = user.getId();
            String name = user.getName();
            String password = user.getPassword();
            String email = user.getEmail();
            int num = user.getNum();
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("insert into myuser (id, name, password, email, num) "
                    + "values ('" + id + "' , '" + name + "' , '" + password + "' ,'" + email + "','" + num + "')");
        } catch (SQLException ex) {
            Logger.getLogger(AddUserServlet.class.getName()).log(
                    Level.SEVERE, null, ex); // 錯誤訊息，但是只有開發人員看的到。
        }
    }
    public void deleteUser(User user) {
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
            Statement stmt = conn.createStatement();
            String id = user.getId();
            stmt.executeUpdate("delete from myuser where id='"+ id +"'");
        }catch (SQLException ex) {
            Logger.getLogger(UserListServlet.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
    }
    public void updataLogin(User user){
        //update login set id=xxx
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
            String id = user.getId();
            String password = user.getPassword();
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("UPDATE myuser SET password = '" 
                    + password +"' WHERE id = '"+ id +"'");
        } catch (SQLException ex) {
            Logger.getLogger(UserListServlet.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
    }
    public List<User> findAll(){
        List<User> ret = new ArrayList<User>();
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("select * from myuser");
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getString("id"));
                user.setName(rs.getString("name"));
                user.setPassword(rs.getString("password"));
                user.setEmail(rs.getString("email"));
                user.setNum(rs.getInt("num"));
                ret.add(user);
            }
        }catch (SQLException ex) {
            Logger.getLogger(UserListServlet.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
    return ret;
    }
    public User findById(String id){
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select * from myuser where id='" +id+"'");
             if(rs.next()){
                User user = new User();
                user.setId(rs.getString("id"));
                user.setPassword(rs.getString("password"));
                return user;
            }
        }catch (SQLException ex) {
                Logger.getLogger(UserListServlet.class.getName()).log(
                        Level.SEVERE, null, ex);
        }
        return null;
    }
    public String findByNum(int num){
        String id = null;
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select id from myuser where num='" +num+"'");
             if(rs.next()){
                id = rs.getString("id");
             }
        }catch (SQLException ex) {
                Logger.getLogger(UserListServlet.class.getName()).log(
                        Level.SEVERE, null, ex);
        }
        return id;
    }
    public int makenum(){
        int max = -1;
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select num from myuser");
             while(rs.next()){
                int num = rs.getInt("num");
                if(num > max){
                    max = num;
                }
            }
        }catch (SQLException ex) {
                Logger.getLogger(UserListServlet.class.getName()).log(
                        Level.SEVERE, null, ex);
        }
        return max + 1;
    }
    public String findIdExict(String id){
        String exict = "0";
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "")) {
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("select id from myuser");
             while(rs.next()){
                String idex = rs.getString("id");
                if(id.equals(idex)){
                    exict = "1";
                }
            }
        }catch (SQLException ex) {
                Logger.getLogger(UserListServlet.class.getName()).log(
                        Level.SEVERE, null, ex);
        }
        return exict;
    }
}
