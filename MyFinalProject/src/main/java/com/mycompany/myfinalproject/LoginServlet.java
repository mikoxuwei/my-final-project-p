/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.mycompany.myfinalproject;

import characterpackage.CharacterDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author miko
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        try ( PrintWriter out = response.getWriter();
                Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/mysql", "root", "");) {
            String id = request.getParameter("id");
            String password = request.getParameter("password");
            Statement stmt = conn.createStatement();
            CharacterDao characterDao = new CharacterDao();
            ResultSet rs = stmt.executeQuery("select * from myuser "
                    + "where id='" +id+ "'");
            if(rs.next()){
                ResultSet rsp = stmt.executeQuery("select * from myuser "
                    + "where password = '" + password + "'");
                if(rsp.next()){
                    session.setAttribute("user", id);
                    if(id.equals("admin") && password.equals("admin")){
                        session.setAttribute("role", "admin");
                        response.sendRedirect("userlist.jsp");
                    }else{
                        int num = rsp.getInt("num");
                        session.setAttribute("role", "user");
                        if(characterDao.findYN(num).equals(0)){
                            characterDao.getnum(num);
                            response.sendRedirect("addchar.jsp");
                        }else{
                            response.sendRedirect("myCharacter?num="+num);
                        }
                    }
                }else{
                    UserDao.e = 4;
                    response.sendRedirect("index.jsp");
                }
            }else{
//                out.println("failed");
                UserDao.e = 3;
                response.sendRedirect("index.jsp");
//                response.sendRedirect("loginerror.jsp");
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
