<%-- 
    Document   : levelup
    Created on : 2022年12月21日, 下午4:44:48
    Author     : miko
--%>
<%@page import="characterpackage.Character"%>
<%@page import="characterpackage.CharacterDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            CharacterDao characterDao = new CharacterDao();
            int id = Integer.parseInt(request.getParameter("id"));
            Character character = characterDao.findById(id);
        %>
        
        Name: <%= character.getId()%> <br/>
        Level: <%=character.getLevel()%> <br/>
            
        <form method="post" action="levelup">
            <input type="submit" value="Update"/>
        </form>
    </body>
</html>
