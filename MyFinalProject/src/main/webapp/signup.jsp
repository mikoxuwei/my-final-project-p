<%-- 
    Document   : signup
    Created on : 2022年12月28日, 下午4:12:24
    Author     : miko
--%>

<%@page import="com.mycompany.myfinalproject.UserDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>註冊</title>
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        />
        <style>
            body {
                display: flex;
                justify-content: center;
                align-items: center;
                min-height: 100vh;
                background: #eceffc;
            }
            .form-input-material {
              --input-default-border-color: white;
              --input-border-bottom-color: white;
            }
            .form-input-material input {
              color: white;
            }
            .login-form {
              display: flex;
              flex-direction: column;
              align-items: center;
              padding: 70px 100px;
              color: white;
              background: rgba(0, 0, 0, 0.8);
              border-radius: 10px;
              box-shadow: 0 0.4px 0.4px rgba(128, 128, 128, 0.109), 0 1px 1px rgba(128, 128, 128, 0.155), 0 2.1px 2.1px rgba(128, 128, 128, 0.195), 0 4.4px 4.4px rgba(128, 128, 128, 0.241), 0 12px 12px rgba(128, 128, 128, 0.35);
            }
            .login-form h1 {
              margin: 0 0 24px 0;
            }
            .login-form .form-input-material {
              margin: 12px 0;
            }
            .login-form div{
                margin-top: 0.8rem;
            }
            a{
                color: #eceffc;
                padding: 5px 15px;
            }
            #draw-border {
                display: flex;
                align-items: center;
                justify-content: center;
                height: 6vh;
                width: 12vh;
                cursor: pointer;
              }
              button {
                margin-top: 1rem; 
                border: 0;
                background: none;
                color: #ffffff;
                font-weight: bold;
                font-size: 90%;
                position: relative;
                outline: none;
                padding: 10px 20px;
                box-sizing: border-box;
                background: rgba(169,169,169,0.5);
              }
              button::before, button::after {
                box-sizing: inherit;
                position: absolute;
                content: '';
                border: 2px solid transparent;
                width: 0;
                height: 0;
              }
              button::after {
                bottom: 0;
                right: 0;
              }
              button::before {
                top: 0;
                left: 0;
              }
              button:hover::before, button:hover::after {
                width: 100%;
                height: 100%;
              }
              button:hover::before {
                border-top-color: #eceffc;
                border-right-color: #eceffc;
                transition: width 0.1s ease-out, height 0.1s ease-out 0.1s;
              }
              button:hover::after {
                border-bottom-color: #eceffc;
                border-left-color: #eceffc;
                transition: border-color 0s ease-out 0.2s, width 0.1s ease-out 0.2s, height 0.1s ease-out 0.3s;
              }
        </style>
        <%
            UserDao userDao = new UserDao();
            int e = userDao.takee();
            if(e == 2){
        %>
            <script type="text/javascript">
                alert("錯誤！此帳號已存在！\n請前往登入，或是選擇其他帳號組合。");
            </script>
        <%    
            }
            userDao.gete(0);
        %>
    </head>
    <body>  
        <form class="login-form" method = "post" action = "addUserServlet">
            <h1>Sign Up</h1>
            <div>
                <label for="name">暱稱</label>
                <input type="text" name = "name" id="name" placeholder=" " autocomplete="off" class="form-input-material" required /><br/>
            </div>
            <div>
                <label for="id">帳號</label>
                <input type="text" name="id" id="id" placeholder=" " autocomplete="off" class="form-input-material" required />     
            </div>
            <div>
                <label for="password">密碼</label>
                <input type="password" name="password" id="password" placeholder=" " autocomplete="new-password" class="form-input-material" required />
            </div>
            <div>
                <label for="email">信箱</label>
                <input type="email" name = "email" id="email" placeholder=" " autocomplete="off" class="form-input-material" required /><br/>
            </div>
            <button type="submit" id="draw-border">註冊</button></br>
            <div><a style="margin-right: 0.5rem" href = "clearServlet">清除</a><span style='color: rgb(128,128,128); margin-left:1.5rem  '>已有帳號？</span><a style="margin-left: 0.05rem" href = "index.jsp">登入</a></div>
        </form>
    </body>
</html>
