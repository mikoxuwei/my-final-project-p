<%-- 
    Document   : characterlist
    Created on : 2022年12月7日, 下午11:52:30
    Author     : miko
--%>

<%@page import="com.mycompany.myfinalproject.UserDao"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="characterpackage.CharacterDao"%>
<%@page import="java.util.List"%>
<%@page import="characterpackage.Character"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Character List</title>
        <style>
            body {
                display: flex;
                flex-grow:0;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                min-height: 90vh;
                background: #eceffc;
            }
            .div-container {
                height: 70vh;
                overflow: auto;
                display: flex;
                flex-direction: column;
                align-items: center;
                padding: 40px 50px;
                width: 80%;
                color: white;
                background: rgba(0, 0, 0, 0.8);
                border-radius: 10px;
                box-shadow: 0 0.4px 0.4px rgba(128, 128, 128, 0.109), 0 1px 1px rgba(128, 128, 128, 0.155), 0 2.1px 2.1px rgba(128, 128, 128, 0.195), 0 4.4px 4.4px rgba(128, 128, 128, 0.241), 0 12px 12px rgba(128, 128, 128, 0.35);
            }
            .table-container {
                width: 100%;
                margin-top: 1rem
            }
            
            th, td {
                width: 14.3%;
                text-align: center;
            }
            th {
                border-bottom:4px solid #ffffff;
                border-right: 1px solid #343a45;
                padding:5px;
                font-size:18px;
                text-align:left;
                text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
                vertical-align:middle;
              }
              td {
                border-bottom: 1px solid #C1C3D1;
                font-size:16px;
                font-weight:normal;
                text-align:left;
                padding:10px;
                text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);
              }
            a{
                color: #eceffc;
                padding: 5px 15px;
                font-size: 18px;
            }
        </style>
    </head>
    <body>
        
        <div class = 'div-container'>
            <div>
                <a href = "userlist.jsp">使用者清單</a>
                <a style='margin-left: 10rem;' href = "logout"> 登出 </a>
            </div>
            <table class="table-container">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>CharacterName</th>
                        <th>Sex</th>
                        <th>Race</th>
                        <th>Rarity</th>
                        <th>Level</th>  
                        <th>Play</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody id="list">
                    <%
                        
                        CharacterDao characterDao = new CharacterDao();
                        List<Character> characters = characterDao.findAll();
                        UserDao userDao = new UserDao();
                        int n = 0;
                        for (Character item : characters) {
                            n = n++;
                    %>
                                <tr>
                                   <td><%= userDao.findByNum(item.getNum()) %></td>
                                   <td><%= item.getName() %></td>
                                   <td><%= item.getSex() %></td>
                                   <td><%= item.getRace() %></td>
                                   <!--<td><%= item.getRarity() %></td>-->
                                   <%
                                       if(item.getRarity().equals("SSR")){
                                   %>
                                            <td><font color="#FF0000">SSR</font></td>
                                   <%
                                       }else if(item.getRarity().equals("SR")){
                                   %>
                                            <td><font color="#FFFF99">SR</font></td>
                                   <%
                                       }else if(item.getRarity().equals("R")){
                                   %>
                                            <td><font color="#00C0FF">R</font></td>
                                   <%
                                       }else{
                                   %>
                                            <td><font color="#D0D0D0">N</font></td> 
                                   <%
                                       }
                                   %>
                                   <td><%= item.getLevel() %></td>                      
                                   <td><a href = 'levelupAll?id=<%= item.getId() %>&level=<%= item.getLevel()%>'>Play</a></td>
                                   <td><a href = 'deleteCharAll?id=<%= item.getId() %>'>Delete</a></td>  
                                </tr>
                                
                    <%                                    
                        }
                    %>
                </tbody>
            </table>
        </div>
                    
    </body>
</html>
