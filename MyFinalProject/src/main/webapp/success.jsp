<%-- 
    Document   : success
    Created on : 2022年12月1日, 下午4:41:22
    Author     : miko
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            body{
                display: flex;
                flex-direction: column;
                align-items: center;
                min-height: 90vh;
                background: #eceffc;
                justify-content: space-evenly;
            }
            a{
                font-size: 18px;
            }
        </style>
        <title>Success</title>
    </head>
    <body>
        <h1>成功創建帳號</h1>
        <div>
            <a href = "index.jsp">前往登入</a><br/>
        </div>
    </body>
</html>
