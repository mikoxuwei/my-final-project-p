<%-- 
    Document   : update
    Created on : 2022年12月28日, 下午8:59:49
    Author     : miko
--%>

<%@page import="com.mycompany.myfinalproject.User"%>
<%@page import="com.mycompany.myfinalproject.UserDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>修改密碼</title>
        <style>
            body {
                display: flex;
                justify-content: center;
                align-items: center;
                min-height: 100vh;
                background: #eceffc;
            }
            .form-input-material {
              --input-default-border-color: white;
              --input-border-bottom-color: white;
            }
            .form-input-material input {
              color: white;
            }
            .login-form {
              display: flex;
              flex-direction: column;
              align-items: center;
              padding: 70px 100px;
              color: white;
              background: rgba(0, 0, 0, 0.8);
              border-radius: 10px;
              box-shadow: 0 0.4px 0.4px rgba(128, 128, 128, 0.109), 0 1px 1px rgba(128, 128, 128, 0.155), 0 2.1px 2.1px rgba(128, 128, 128, 0.195), 0 4.4px 4.4px rgba(128, 128, 128, 0.241), 0 12px 12px rgba(128, 128, 128, 0.35);
            }
            .login-form h1 {
              margin: 0 0 24px 0;
            }
            .login-form .form-input-material {
              margin: 12px 0;
            }
            .login-form div{
                margin-top: 0.8rem;
            }
            a{
                color: #eceffc;
                padding: 5px 15px;
            }
            #draw-border {
                display: flex;
                align-items: center;
                justify-content: center;
                height: 6vh;
                width: 12vh;
                cursor: pointer;
              }
              button {
                margin-top: 1rem; 
                border: 0;
                background: none;
                color: #ffffff;
                font-weight: bold;
                font-size: 90%;
                position: relative;
                outline: none;
                padding: 10px 20px;
                box-sizing: border-box;
                background: rgba(169,169,169,0.5);
              }
              button::before, button::after {
                box-sizing: inherit;
                position: absolute;
                content: '';
                border: 2px solid transparent;
                width: 0;
                height: 0;
              }
              button::after {
                bottom: 0;
                right: 0;
              }
              button::before {
                top: 0;
                left: 0;
              }
              button:hover::before, button:hover::after {
                width: 100%;
                height: 100%;
              }
              button:hover::before {
                border-top-color: #eceffc;
                border-right-color: #eceffc;
                transition: width 0.1s ease-out, height 0.1s ease-out 0.1s;
              }
              button:hover::after {
                border-bottom-color: #eceffc;
                border-left-color: #eceffc;
                transition: border-color 0s ease-out 0.2s, width 0.1s ease-out 0.2s, height 0.1s ease-out 0.3s;
              }
        </style>
    </head>
    <body>
        <%
            UserDao dao = new UserDao();
            User user = dao.findById(request.getParameter("id"));
        %>       
        <form class="login-form"  method = "post" action = "update">
            <div>
                <h1>Reset password</h1>
              <label for="username">帳號</label>
              <input type="text" readonly='readonly' name="id" id="username" value="<%= user.getId()%>" placeholder=" " autocomplete="off" class="form-input-material" required />     
            </div>
            <div>
              <label for="password">密碼</label>
              <input type="password" name="password" id="password" value="<%= user.getPassword()%>" placeholder=" " autocomplete="off" class="form-input-material" required />
            </div>
            <button type="submit" id="draw-border">修改</button></br>
            <div><span style='color: rgb(128,128,128)'>不想改了？</span><a style="margin-left: 0.3rem" href = "userlist.jsp">返回</a></div>
        </form>
    </body>
</html>
