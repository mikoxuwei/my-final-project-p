<%-- 
    Document   : userlist
    Created on : 2022年12月28日, 下午4:50:56
    Author     : miko
--%>

<%@page import="com.mycompany.myfinalproject.User"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.myfinalproject.UserDao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>User List</title>
        <style>
            body {
                display: flex;
                flex-grow:0;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                min-height: 90vh;
                background: #eceffc;
            }
            .div-container {
                height: 70vh;
                overflow: auto;
                display: flex;
                flex-direction: column;
                align-items: center;
                padding: 40px 50px;
                width: 80%;
                color: white;
                background: rgba(0, 0, 0, 0.8);
                border-radius: 10px;
                box-shadow: 0 0.4px 0.4px rgba(128, 128, 128, 0.109), 0 1px 1px rgba(128, 128, 128, 0.155), 0 2.1px 2.1px rgba(128, 128, 128, 0.195), 0 4.4px 4.4px rgba(128, 128, 128, 0.241), 0 12px 12px rgba(128, 128, 128, 0.35);
            }
            .table-container {
                width: 100%;
                margin-top: 1rem
            }
            th, td {
                width: 13.3%;
                text-align: center;
            }
            th {
                border-bottom:4px solid #ffffff;
                border-right: 1px solid #343a45;
                padding:10px;
                text-align:left;
                text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
                vertical-align:middle;
                font-size:18px;
              }
              td {
                border-bottom: 1px solid #C1C3D1;
                font-size:16px;
                font-weight:normal;
                text-align:left;
                padding:10px;
                text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);
              }
            a{
                color: #eceffc;
                padding: 5px 15px;
            }
            .lon{
                width: 20%;
            }
            .lon-c{
                width: 10%;
                font-size: 16px;
            }
        </style>
    </head>
    <body>
        <div class = 'div-container'>
            <div>
                <a style='font-size: 18px;' href = 'logout'>登出</a>
                <a style='margin-left: 10rem; font-size: 18px;' href = 'characterlist.jsp'>全遊戲角色</a>
            </div>
            <table class="table-container">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Id</th>
                        <th>Password</th>
                        <th class="lon">Email</th>
                        <th class="lon-c">修改密碼</th>
                        <th class="lon-c">刪除使用者</th>
                        <th class="lon-c">角色清單</th>
                        <!--<th>Num</th>-->
                    </tr>
                </thead>
                <tbody id="list">
                    <%
                        UserDao userDao = new UserDao();
                        List<User> users = userDao.findAll();
                        int n = 0;
                        for (User item : users) {
                    %>
                                <tr>
                                   <td><%= item.getName() %></td>
                                   <td><%= item.getId() %></td>
                                   <td><%= item.getPassword()%></td>
                                   <td class="lon"><%= item.getEmail()%></td>
                                   <!--<td><%= item.getNum()%></td>-->
                                   <td class="lon-c"><a href = 'update.jsp?id=<%= item.getId() %>&password=<%= item.getPassword() %>'>修改</a></td>
                                   <td class="lon-c"><a href = 'deleteUser?id=<%= item.getId() %>&num=<%= item.getNum()%>'>刪除</a></td>
                                   <!--<td><a href = 'characterlist.jsp?id=<%= item.getId()%>'>遊戲角色</a></td>-->
                                   <td class="lon-c"><a href = 'myCharacter?num=<%= item.getNum()%>'>角色</a></td>
                                </tr>
                                
                    <%                                    
                        }
                    %>
                </tbody>
            </table>
        </div>
    </body>
</html>
