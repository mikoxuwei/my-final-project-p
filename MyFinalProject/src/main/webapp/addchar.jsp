<%@page import="characterpackage.CharacterDao"%>
<%@page import="java.util.List"%>
<%@page import="com.mycompany.myfinalproject.User"%>
<%@page import="characterpackage.Character"%>
<%@page contentType = "text/html; charset=UTF-8" language = "Java" %>
<!DOCTYPE html>
<html>
    <head>
        <title>創建角色</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            body {
                display: flex;
                justify-content: center;
                align-items: center;
                min-height: 100vh;
                background: #eceffc;
            }
            .form-input-material {
              --input-default-border-color: white;
              --input-border-bottom-color: white;
            }
            .form-input-material input {
              color: white;
            }
            .login-form {
              display: flex;
              flex-direction: column;
              align-items: center;
              padding: 70px 100px;
              color: white;
              background: rgba(0, 0, 0, 0.8);
              border-radius: 10px;
              box-shadow: 0 0.4px 0.4px rgba(128, 128, 128, 0.109), 0 1px 1px rgba(128, 128, 128, 0.155), 0 2.1px 2.1px rgba(128, 128, 128, 0.195), 0 4.4px 4.4px rgba(128, 128, 128, 0.241), 0 12px 12px rgba(128, 128, 128, 0.35);
            }
            .login-form h1 {
              margin: 0 0 24px 0;
            }
            .login-form .form-input-material {
              margin: 12px 0;
              margin-left: 1rem;
            }
            a{
                color: #eceffc;
                font-size: 18px;
            }
            #draw-border {
                display: flex;
                align-items: center;
                justify-content: center;
                height: 6vh;
                width: 12vh;
                cursor: pointer;
              }

              button {
                margin-top: 2rem; 
                border: 0;
                background: none;
                /*text-transform: uppercase;*/
                /*color: #4361ee;*/
                color: #ffffff;
                font-weight: bold;
                font-size: 90%;
                position: relative;
                outline: none;
                padding: 10px 20px;
                box-sizing: border-box;
                background: rgba(169,169,169,0.5);
              }

              button::before, button::after {
                box-sizing: inherit;
                position: absolute;
                content: '';
                border: 2px solid transparent;
                width: 0;
                height: 0;
              }
              button::after {
                bottom: 0;
                right: 0;
              }
              button::before {
                top: 0;
                left: 0;
              }
              button:hover::before, button:hover::after {
                width: 100%;
                height: 100%;
              }
              button:hover::before {
                border-top-color: #eceffc;
                border-right-color: #eceffc;
                transition: width 0.1s ease-out, height 0.1s ease-out 0.1s;
              }
              button:hover::after {
                border-bottom-color: #eceffc;
                border-left-color: #eceffc;
                transition: border-color 0s ease-out 0.2s, width 0.1s ease-out 0.2s, height 0.1s ease-out 0.3s;
              }
            select {
                box-shadow: none;
                flex: 1;
              }
                .select{
                  position: relative;
                  display: inline-block;
                  margin-bottom: 15px;
                  margin-left: 1rem;
                  width:75%;
                }
                  select{
                    display:inline-block;
                    width:100%;
                    cursor:pointer;
                    padding:5px 10px;
                    outline:0;
                    border:0;
                    border-radius:0;
                    background: #fff;
                    color: #343a45;
                  }
                    .select ::-ms-expand{
                      display: none;
                    }
                    .select :hover,
                    .select :focus{
                      color: black;
                      background: #fff;
                      }
                    .select :disabled{
                      opacity:0.5;
                      pointer-events:none;
                    }
                .select__arrow{
                  position:absolute;
                  color: black;
                  top:1px;
                  right:1px;
                  width:0;
                  height:0;
                  pointer-events:none;
                  border-style:solid;
                  border-width:8px 5px 0 5px;
                  border-color:rgb(210,210,210);
                }
                  .select select:hover ~ &
                  .select select:focus ~ &{
                    border-top-color: black;
                  }
                  .select select:disabled ~ &{
                    border-top-color:grey;
                }
        </style>
    </head>
    
    <body>   
        <form class="login-form" method = "post" action = "addCharacterServlet">
            <div>
                <!--<a href = "clearCharacter"> 清除 </a>-->
            <!--<a href = "loginServlet"> 登入 </a><br/>-->
               <a style='margin-right: 1rem;' href = "mycharacterlist.jsp"> 角色清單 </a>
               <h1 style="margin-top: 2.5rem;">New Character</h1>
            </div> 
            <div style="">
                <div> 
                    <label for="name">名字</label>
                      <input style="width: 70%; padding: 3px 3.5px" type="text" name="name" id="name" placeholder=" " autocomplete="off" class="form-input-material" required />     
                </div>
                <div>
                    <label>性別</label>
                    <label><input type="radio" name="sex" class="form-input-material" value="male" required> 男</label>
                    <label><input type="radio" name="sex" class="form-input-material" value="female"> 女</label>
                    <label><input type="radio" name="sex" class="form-input-material" value="else"> 其他</label>
                </div>
                <div style='margin-top: 1rem'>            
                    <label for="race">種族</label>
                    <div class="select">
                    <select name="race" id="race">
                        <!-- <optgroup label="攻擊"> -->
                        <option value="human"> 人族</option>
                        <option value="cat"> 獸人族</option>
                        <!-- </optgroup>
                        <optgroup label="輔助">  -->
                        <option value="dwarf"> 矮人族</option>
                        <option value="elf"> 精靈族</option>
                         <!-- </optgroup> -->
                    </select> 
                    </div>
                </div>
                <div style="margin-top: 0.5rem">
                    稀有度 --- 未知 
                </div>
                <div style='margin-top: 1.5rem'>
                    等級 ------ 1
                </div>
            </div>    
            <button type="submit" id="draw-border">創建</button>
        </form>
    </body>
</html>

